# recommendation-test

## How to install
 
Install nodejs (this API was tested on v8.9.1), git and npm.   
Clone this repository to your local computer. Then do: 
```bash
npm install
```

Then to start the server: 

```bash 
npm start
```

Note that every data is stored in RAM, so if you restart the server you will loose all the data. 

## The application

This REST api stores items and their recommendations. 

The API is built on top of nodejs and [express](http://expressjs.com/de/).

The base object is Item, it contains a name (string), a description (string), several tags, and several recommendations. 

A tag is simply a string. A recommendation has an author, a message and a score (from 0 to 10), and a date of creation.

Based on the score of its recommendations, an item has also a score. 

Item and recommendations also have an ID. 

## Your task

Your task is simple: 

Create a custom authentication layer on top of this API. Please prepare two routes:

- registration route for users (mail / password)
- login route

*Please do NOT use a prebuild library*

You can just store all the data in ram & don't need to use a database. 

Make sure, that all routes of the API are only accessible for authenticated users. 

*Please do not take more than 2-3 hours to complete the task.*

We do not expect you to build a full-blown authentication layer, but would like to see that you are able to decide, which aspects are most relevant.

Nevertheless, you should focus on making it as secure as possible.

Please upload your results to a new github or bitbucket repository and send us the link. 

## What we expect

We want to see:

- that you are able to write clean code
- that you understand the concepts of node.js & express
- that you are able to quickly learn the basics of new frameworks
- that you can identify the necessary & the most relevant steps in a limited time
- that you understand how to secure a REST api. 
