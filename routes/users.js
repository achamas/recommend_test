const express = require('express');
const router = express.Router();

const User = require('../models/user');
const bcrypt = require('bcryptjs');
var passport = require('passport');
//Register form
router.get('/registeration', function(req, res){
    res.render('registeration');

});

//Register

router.post('/registeration', function(req, res){
    var name = req.body.name;
    var email = req.body.email;
    var username = req.body.username;
    var password = req.body.password;
    var confpassword = req.body.confpassword;

    req.checkBody('Name', 'Name is required').notEmpty();
    req.checkBody('email', 'Email is not valid').isEmail();
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('username', 'username is required').notEmpty();
    req.checkBody('password', 'Name is required').notEmpty();
    req.checkBody('confpassword', 'confpassword is not matched').equal(req.body.password);



        var errors = req.validationErrors();

        if(errors){
            res.render('register',{
                errors:errors
            });
        } else {
            var newUser = new User({
                name: name,
                email:email,
                username: username,
                password: password
            });

            User.createUser(newUser, function(err, user){
                if(err) throw err;
                console.log(user);
            });

            req.flash('success_msg', 'You are registered and can now login');

            res.redirect('/users/login');
        }
    });

router.post('/login',
    passport.authenticate('local', {successRedirect:'/', failureRedirect:'/users/login',failureFlash: true}),
    function(req, res) {
        res.redirect('/');
    });
//log in

router.get('/login', function(req, res){
    res.render('login');
});
module.exports = router;