const express = require('express');
const router = express.Router();
const Item = require('../models/item');


router.get('/', function (req, res, next) {
	if (req.query.tags)
		res.send(Item.getByTag(req.query.tags));
	else
		res.send(Item.getAll());
});


router.get('/:id', function (req, res, next) {
	const ret = Item.get(req.params.id);
	if (!ret) {
		const err = new Error(`Document with ID ${req.params.id} does not exist.`);
		err.status = 404;
		throw err;
	}

	else {
		res.send(ret);
	}
});


router.put('/:id', function (req, res, next) {
	const ret = Item.update(req.params.id, req.body);
	if (!ret) {
		const err = new Error(`Document with ID ${req.params.id} does not exist.`);
		err.status = 404;
		throw err;
	}

	else {
		res.send(ret);
	}
});

router.post('/', function (req, res, next) {
	if (!req.body.name || !req.body.description || !req.body.tags) {
		const err = new Error('Missing field in document' + req.body);
		err.status = 400;
		throw err;
	}

	res.send(Item.create(req.body.name, req.body.description, req.body.tags));
});

router.post('/:id/vote/', function (req, res, next) {
	const ret = Item.get(req.params.id);
	if (!ret) {
		const err = new Error(`Document with ID ${req.params.id} does not exist.`);
		err.status = 404;
		throw err;
	}

	const score = parseInt(req.body.score);
	if (!score || score < 0 || score > 10) {
		const err = new Error('Score should be between 0 and 10');
		err.status = 400;
		next(err);
	}
	if (!req.body.author || !req.body.message) {
		const err = new Error('Missing field in document' + req.body);
		err.status = 400;
		throw err;
	}

	ret.recommends(req.body.author, req.body.message, score);
	res.send(ret.toJSON());
});

module.exports = router;
