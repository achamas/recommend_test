class user {
    constructor (name, email, username, password) {
        this._id = 'U' + user.ID;
        this.name = name;
        this.email = email;
        this.username = username;
        this.password = password;
    }

    toJSON () {
        return {
            _id: this._id,
            name: this.name,
            email: this.email,
            username: this.username,
            password: this.password,
            createdAt: this.createdAt
        };
    }

    static get ID () {
        const _id = user.__id;
        user.__id++;
        return _id;
    }
}

user.__id = 0;

module.exports = user;