class Recommendation {
	constructor (author, message, score) {
		this._id = 'R' + Recommendation.ID;
		this.author = author;
		this.message = message;
		this.score = score;
		this.createdAt = new Date();
	}

	toJSON () {
		return {
			_id: this._id,
			author: this.author,
			message: this.message,
			score: this.score,
			createdAt: this.createdAt
		};
	}

	static get ID () {
		const _id = Recommendation.__id;
		Recommendation.__id++;
		return _id;
	}
}

Recommendation.__id = 0;

module.exports = Recommendation;