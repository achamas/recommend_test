const express = require('express');
const logger = require('morgan');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const index = require('./routes/index');
const ejs = require('ejs');
const session = require('express-session');
const engine = require('ejs-mate');
var passport = require('passport');
const path = require('path');
const app = express();

var routes = require('./routes/index');
var users = require('./routes/users');

app.engine('ejs', engine);
app.set('view engine', 'engine');
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.get('/' , function (req,res) {
	res.sendFile(__dirname+ '/public/index.html');

});

app.get('/registration' , function (req,res) {
    res.sendFile(__dirname+ '/public/registration.html');

});

app.get('/registrations' , function (req,res) {
    res.sendFile(__dirname+ '/public/registration.html');

});
app.get('/login' , function (req,res) {
    res.sendFile(__dirname+ '/public/login.html');

});
app.post(
		'/registration', function (req,res)
	{
    var Name = req.body.Name;
    var email = req.body.email;
    var username = req.body.username;
    var password = req.body.password;
    var confpassword = req.body.confpassword;
    console.log(Name);
    console.log(email);
    console.log(username);
    console.log(password);
    console.log(confpassword);

})

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.set('view engine', 'html');


app.use('/', index);

// error handler
app.use(function (err, req, res, next) {
	// set locals, only providing error in development
	if (req.app.get('env') === 'development')
		console.error(err);
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.send(err);
});

// Passport init
app.use(passport.initialize());
app.use(passport.session());

// express session
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));

app.use('/', routes);
app.use('/users', users);

module.exports = app;
// Start Server
app.listen(3000, function() {
    console.log('Server started on port 3000...');
});